#
#     Copyright (c) 2023 NetCraftsmen
#     All rights reserved.
#
#     author: @joelwking
#     written:  13 May 2021
#     references:
#       activate virtualenv: https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
#
FROM python:3.8.10-slim-buster
ENV VIRTUAL_ENV=/opt/ansible
LABEL maintainer="Joel W. King" email="programmable.networks@gmail.com"
RUN apt update && \
    apt -y install git && \
    apt -y install python3-venv && \
    pip3 install --upgrade pip 
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
#
# We need the requirements.txt and the requirements.yml files for installation
#
RUN mkdir /vscode
COPY . /vscode
WORKDIR /vscode
RUN pip install -r requirements.txt && \
    ansible-galaxy collection install -r collections/requirements.yml && \
    ansible-galaxy role install -r roles/requirements.yml
#
#   The ansible collection(s) by default will install in /root/.ansible/
#
#   The virtual environment is /opt/ansible
#
#   The work directory is /vscode
#
#   And, finally, the underlying directory is /workspaces/vs_code_remote_container