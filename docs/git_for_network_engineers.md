# Introduction to Git for Network Engineers

These instructions assume you have installed **Git** for Windows <https://gitforwindows.org/> or installed Git `brew install git` after installing [Homebrew](https://brew.sh/) for MacOS.

The `../README.md` file has instructions for installing VS Code and Docker Desktop on the laptop. Docker Desktop must be running, if not, start it.

## Signup for Github/GitLab accounts

Sign-up here: https://www.github.com and choose the Free plan (default). Also sign-up for GitLab
(https://gitlab.com) as a personal account and register with the same handle used with GitHub.

## Verify Installation

On Windows laptops, launch the **Git Bash shell**. On MacOS, open a terminal window.

Verify your Git installation.

```shell
git --version
```

Use the `git config` command to set your name and email address. These values are used to identify the author of changes.

```shell
git config --global user.name "Ben Dover"
git config --global user.email ben@example.net
git config --global core.editor vim # Note esc + :q or esc + :q! to exit vim
git config --global color.ui true
```

Tip: If you need to disable SSL verification, use `git config --global http.sslVerify false`.

The global configuration entries are viewed by using:

```shell
git config --global --list
```
You can view the global config file in your home directory.

```shell
cat ~/.gitconfig
```

## Clone the sample repository

Create and enter a directory (perhaps **~/Documents/nc/projects/**) where you wish to store your local repositories and issue the **git clone** command.

```shell
git clone https://gitlab.com/joelwking/vs_code_remote_container.git
```

>Tip: `git clone` is basically a macro that does five separate things; creates a directory with the same name as the repository we created, issues a git init, to initialize the local repository, issues a git remote command named ‘origin’ pointing to the URL specified in the clone, downloads all the content from the remote repository using git fetch, issues a git checkout of the default branch.

Enter the directory created by the **clone** command.

```shell
cd vs_code_remote_container
```

>Note: each repository also has a configuration file `cat .git/config`

View the local configuration entries.

```shell
git config --local --list
```

Configuration changes can be *global* or *local* (to the current repository).

## Basic Commands

After cloning the demo repository, a change to an existing file, or a new file is added, the change is added to the staging area, committed and then pushed to a remote repository under your own repository on GitLab.

This provides an opportunity for continued self-study using your own independent copy of the training repository.

### git pull

The `git pull` command updates the local computer files to have the most up-to-date version of the remote repository. You **pull** the changes down from GitHub / GitLab with this command.

One of the functions of the `git clone` command is to issue a `git pull`. After the repository is cloned, you issue the pull command to update the local computer files with the most up-to-date version of the remote repository.

```shell
git pull
```

## Start VS Code

One function of the `git clone` command is to create a directory with the same name as the remote repository.

Enter this directory `cd vs_code_remote_container` and start VS Code from your desktop.

When prompted to open the directory / workspace in a container, affirm that option.

>Tip: If VS Code is in your PATH, after entering the directory, you can issue `code .` from the command line to start VS Code.

### Modify a file

Using VS Code, open the `README.md` file, scroll to the bottom of the file, and change the author to your name and GitHub/GitLab userid. Save the file.

Exit VS Code by opening the tab **File -> Close Remote Connection**.

## Git Commands

From the command line, issue a `git status` to view the state of the working directory.

### git status

The git status command is used frequently. Reporting on the status of the repository, what files have been created and modified, which branch you are on, etc.

```shell
git status
```

It will indicate the `README.md` file was modified.

### git add

The `git add` command adds a change in the working directory to the staging area. It tells Git that you want to include updates to a particular file in the next commit.

```shell
git add README.md
```

Issue a `git status` command to see the changes to be committed.

### git remote

Think of `git remote` as a means of creating an alias for a URL, in this example, *origin* is the alias for the remote repository we created earlier.

>Tip: One of the functions of the `git clone` command is to issue a `git remote` command named **origin** pointing to the URL specified in the clone command.

Issue the `git remote` command to view the current configuration.

```shell
git remote -v
```

Issue the `git branch` command and identify the name of the default branch. The default branch name is typically be `master` or `main`. If there are multiple branches, the active branch has an asterisk to the left of the name.

```shell
git remote add -m main training https://gitlab.com/<your_git_userid>/vs_code_remote_container.git
```

>Tip: Issue the `git remote -v` to view your changes.

The local configuration now has two aliases, **origin** and **training**, each with a URL associated with them.

### git commit

The `git commit` commits the staged snapshot to the project history.

```shell
git commit -m 'lab exercise'
```

### git log

The `git log` command shows a list of commits for this project, a history of the changes.

```shell
git log --oneline
```

>Tip: Issue the `git log` command without the **--oneline** argument. Observe the **Author** of the commit includes the Name and Email address from the global configuration. 


### git push

Now that we have created a new file and use the commit command to sync it from the staging area to the local repository, we can **push** this change to the remote (GitHub / GitLab).

>Tip: Use the `git status` command is view the current branch and other information.

```shell
git push training master
```

Enter your **username** and **password** for your GitLab (or GitHub) account when prompted. If not prompted, your system may have **terminal prompts disabled**, look for a browser window prompting for your GitLab userid and password. The window may be hidden behind your active window.

## Re-configure remote

Now that you have pushed to your personal remote, modify the local configuration to eliminate the reference to the original remote repository.

```shell
git remote remove origin
git remote rename training origin
git remote -v
```

Using the above commands swapped the URL associated with the *training* alias with the *origin* alias.

Future changes can be pushed to your remote by using the `git push origin master` command.

## Remote Repository

Login to your GitLab account. View all projects from the dashboard **https://gitlab.com/dashboard/projects**.  Select the project **vs_code_remote_container**. In the *general* settings, create a project description and upload a project avatar. Open an **issue** against the project, assign it to yourself. Create a *Snippet*. View the *Activity* and *Members*.

## Questions

The following questions can be used to assess your Git knowledge. Not all answers have been provided in this exercise. You may need to do additional research.

 * What are the 5 actions of the `git clone` command?
 * Is **origin** a ***magic*** name with special significance or simply a convention?
 * What information is stored in the **.git** directory after a remote repository is cloned?
 * What is the difference between a Snippet and a Gist? Describe their value or why they may be used.
 * What is the purpose of the **.gitignore** file?
 * What Git command is used to view the differences in a file at different points in time?
 * What Open-Source company office occupied offices at the base of the Lucky Strike Smokestack in Durham NC?

## Answers

To submit your answers for the questions, enter the answers in this section and push them to your remote repository. Include your GitHub and GitLab userid in the *author* section below.

Create a **GitHub** profile using a markdown document in the [special repository.](https://docs.github.com/en/account-and-profile/setting-up-and-managing-your-github-profile/customizing-your-profile/about-your-profile)

Update your GitHub and GitLab profile with a photo other than the default generated avatar.

Email or IM <jking@netcraftsmen.com> with the URL of your copy of the GitLab repository.

## Issues

If you locate any typographic or other errors, open an issue at https://gitlab.com/joelwking/vs_code_remote_container/-/issues to provide details. 

## Author

Joel W. King @joelwking