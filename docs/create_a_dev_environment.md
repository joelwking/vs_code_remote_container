
Create a directory and workspace
--------------------------------

Create a local directory, or create a new remote repository on GitHub / GitLab and clone it to your local machine.

Select Code -> File -> Save Workspace As

Add the current folder to the workspace.

Create User and Workspace settings
----------------------------------

Select the Gear Icon on the menu (bottom) and Select Settings (https://code.visualstudio.com/docs/getstarted/settings)

You can modify the User, Workspace and Folder settings.

Settings have precedence: (from most to least)

Workspace Folder
Workspace setting
User settings

If you modify the workspace settings, they will be saved in your workspace.code-workspace file

Create the `.devcontainer` folder
---------------------------------

Create (or save a copy from this repository) the `.devcontainer` folder and `devcontainer.json` files.  You may wish to change the name value in the configuration file and add or update other values. Specify the name of your Dockerfile. The Dockerfile can be in the `.devcontainer` directory. However, putting the `Dockerfile` in the root of the folder allows your to copy the requirements file(s).

Create the Dockerfile
---------------------

Copy or save the Dockerfile into your folder. The sample `Dockerfile` creates a python virtual environment in the container and installs Ansible in the virtual environment. It also upgrades `pip`, installs the python packages in `requirements.txt` and uses `ansible-galaxy` to install any necessary collections or roles.

Examine and modify the respective requirement file(s). Modify the name of the Python venv by updating the `ENV VIRTUAL_ENV` line.

(Re)Build and Open the container
--------------------------------

Select the Gear Icon and select the Command Palette (or click the `><`) icon in the lower left and type in `remote` in the dialog box. Select `Remote-Container: Rebuild and Reopen in Container`  Select the `show log` in the lower right corner to observe the steps of building and running the container.

If the build is successful, open a new terminal window. You should should see a prompt similar to:

```shell
root@ff9c3cca27f2:/workspaces/vs_code_remote_container
```

Issue a `ls -salt` command in the prompt, all the files in your local folder should appear. In the lower left corner of VS Code, you should see the name you entered in the `.devcontainer/devcontainer.json` configuration file.

From the terminal prompt, issue `touch CREATEDINCONTAINER` and note the file appears in the Explorer pane. The file created and edited in the container appears on your local system and will exist after the container is stopped or destroyed.

Open a new terminal. From the terminal prompt, issue `ansible --version`. 

```shell
(ansible) root@9cb83eb66a32:/workspaces/vs_code_remote_container/playbooks# ansible --version
ansible [core 2.13.4]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/ansible/lib/python3.8/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /opt/ansible/bin/ansible
  python version = 3.8.10 (default, Jun 23 2021, 11:28:12) [GCC 8.3.0]
  jinja version = 3.1.2
  libyaml = True
```
Issue `ansible-galaxy collection list` to view the collections installed.

```shell
(ansible) root@9cb83eb66a32:/workspaces/vs_code_remote_container/playbooks# ansible-galaxy collection list

# /root/.ansible/collections/ansible_collections
Collection        Version
----------------- -------
community.general 3.5.0  
joelwking.mongodb 0.0.3  

# /opt/ansible/lib/python3.8/site-packages/ansible_collections
Collection                    Version
----------------------------- -------
amazon.aws                    3.4.0  
ansible.netcommon             3.1.0  
ansible.posix                 1.4.0  
ansible.utils                 2.6.1  
ansible.windows               1.11.0 
arista.eos                    5.0.1  
awx.awx                       21.4.0 
azure.azcollection            1.13.0 
check_point.mgmt              2.3.0  
chocolatey.chocolatey         1.3.0  
cisco.aci                     2.2.0  
cisco.asa                     3.1.0  
cisco.dnac                    6.5.3  
cisco.intersight              1.0.19 
cisco.ios                     3.3.0  
cisco.iosxr                   3.3.0  
cisco.ise                     2.5.0  
cisco.meraki                  2.10.1 
cisco.mso                     2.0.0  
cisco.nso                     1.0.3  
cisco.nxos                    3.1.0  
cisco.ucs                     1.8.0  
cloud.common                  2.1.2  
cloudscale_ch.cloud           2.2.2  
community.aws                 3.5.0  
community.azure               1.1.0  
community.ciscosmb            1.0.5  
community.crypto              2.5.0  
community.digitalocean        1.21.0 
community.dns                 2.3.1  
community.docker              2.7.1  
community.fortios             1.0.0  
community.general             5.5.0  
community.google              1.0.0  
community.grafana             1.5.2  
community.hashi_vault         3.2.0  
community.hrobot              1.5.2  
community.libvirt             1.2.0  
community.mongodb             1.4.2  
community.mysql               3.4.0  
community.network             4.0.1  
community.okd                 2.2.0  
community.postgresql          2.2.0  
community.proxysql            1.4.0  
community.rabbitmq            1.2.2  
community.routeros            2.2.1  
community.sap                 1.0.0  
community.sap_libs            1.2.0  
community.skydive             1.0.0  
community.sops                1.3.0  
community.vmware              2.8.0  
community.windows             1.11.0 
community.zabbix              1.8.0  
containers.podman             1.9.4  
cyberark.conjur               1.1.0  
cyberark.pas                  1.0.14 
dellemc.enterprise_sonic      1.1.1  
dellemc.openmanage            5.5.0  
dellemc.os10                  1.1.1  
dellemc.os6                   1.0.7  
dellemc.os9                   1.0.4  
f5networks.f5_modules         1.19.0 
fortinet.fortimanager         2.1.5  
fortinet.fortios              2.1.7  
frr.frr                       2.0.0  
gluster.gluster               1.0.2  
google.cloud                  1.0.2  
hetzner.hcloud                1.8.1  
hpe.nimble                    1.1.4  
ibm.qradar                    2.0.0  
ibm.spectrum_virtualize       1.9.0  
infinidat.infinibox           1.3.3  
infoblox.nios_modules         1.3.0  
inspur.sm                     2.0.0  
junipernetworks.junos         3.1.0  
kubernetes.core               2.3.2  
mellanox.onyx                 1.0.0  
netapp.aws                    21.7.0 
netapp.azure                  21.10.0
netapp.cloudmanager           21.19.0
netapp.elementsw              21.7.0 
netapp.ontap                  21.22.0
netapp.storagegrid            21.10.0
netapp.um_info                21.8.0 
netapp_eseries.santricity     1.3.1  
netbox.netbox                 3.7.1  
ngine_io.cloudstack           2.2.4  
ngine_io.exoscale             1.0.0  
ngine_io.vultr                1.1.2  
openstack.cloud               1.8.0  
openvswitch.openvswitch       2.1.0  
ovirt.ovirt                   2.2.3  
purestorage.flasharray        1.13.0 
purestorage.flashblade        1.9.0  
purestorage.fusion            1.0.2  
sensu.sensu_go                1.13.1 
servicenow.servicenow         1.0.6  
splunk.es                     2.0.0  
t_systems_mms.icinga_director 1.31.0 
theforeman.foreman            3.4.0  
vmware.vmware_rest            2.2.0  
vyos.vyos                     3.0.1  
wti.remote                    1.0.4  
```
>Note: the Ansible Python module is the Python virtual environment and the Ansible executable is within the virtual environment as well.

The Ansible collection(s) by default are installed in `/root/.ansible/`.

Execute a test playbook in `playbooks/test.yml`.

```shell
(ansible) root@9cb83eb66a32:/workspaces/vs_code_remote_container# cd playbooks/
(ansible) root@9cb83eb66a32:/workspaces/vs_code_remote_container/playbooks# ansible-playbook test.yml
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit
localhost does not match 'all'

PLAY [test] *************************************************************************************

TASK [hello world] ******************************************************************************
ok: [localhost] => {
    "msg": "hello world"
}

PLAY RECAP **************************************************************************************
localhost                  : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```

>Note: execute again with two levels of verbosity to see the paths, e.g. `ansible-playbook test.yml -vv`

Docker Desktop
--------------

Open your Docker Deskop window and examine the Docker image and container which has been created.

Close the Remote Connection
---------------------------

When finished, you can close the remote connection by clicking the `><` icon and selecting that option.

Re-open the Remote Container
----------------------------

Select the Command Palette and `Open Workspace in Remote Container`. The container will be restarted and accessible again. 


Author
------

Joel W. King @joelwking