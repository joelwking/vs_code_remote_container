# vs_code_remote_container

__Instructions for using VS Code Remote Explorer for a container__

These instructions assume you have a computer running the macOS, Linux, and Windows 10 operating systems and have previously installed:

* **Docker Desktop**: Download and install Docker Desktop for your target development environment, <https://www.docker.com/products/docker-desktop>
* **Visual Studio Code**: Download and install <https://code.visualstudio.com/download>
* **Git for Windows**: On MS Windows machines, install **Git** for Windows <https://gitforwindows.org/>

## Visual Studio Code Remote Development Extension Pack

Install the [Visual Studio Code Remote Development Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack).

Select the 'Extensions' sidebar menu item and enter `remote` in the Search Extensions in Marketplace dialog box and install this extension (ms-vscode-remote.remote-containers).

You can also locate and install the **Visual Studio Code Remote - Containers** <https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers> from the web link.

> Note: You may need to restart VS code after installing the Extension Pack for the extension to be recognized.

## Next Steps

For those attending my Third Thursday Tech Talk (TTTT - 16 March 2023) follow the instructions for a Git exercise for Network Engineers available at [docs/git_for_network_engineers.md](./docs/git_for_network_engineers.md).

If you would like explore creating and customizing your own development environment, there are instructions at [docs/create_your_dev_environment.md](./docs/create_a_dev_environment.md).

## Remote container development with VS Code and Podman

[Podman](https://podman.io/) is a daemonless, rootless container engine developed by RedHat, designed as an alternative to Docker. Podman is open source and free, while Docker Desktop for enterprise use requires a monthly or annual license. 

Refer to this article on VS Code and Podman Desktop. <https://developers.redhat.com/articles/2023/02/14/remote-container-development-vs-code-and-podman>

Author
------

Joel W. King @joelwking